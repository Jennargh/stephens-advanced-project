var map; 

var runTimeData = [];

var mapOptions = {
    center: new google.maps.LatLng(37.7831,-122.4039),
    zoom: 14,
    mapTypeId: google.maps.MapTypeId.ROADMAP
};

new google.maps.Map(document.getElementById('map'), mapOptions);

function initialise(location){
	console.log(location);

	//set up map and map options
	var currentLocation = new google.maps.LatLng(location.coords.latitude, location.coords.longitude)

	var mapOptions = {
	  center: currentLocation,
	  zoom: 10,
	  mapTypeId: google.maps.MapTypeId.ROADMAP
	};

	//Create the map and add a geolocation marker on it.
	map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);

	var marker = new google.maps.Marker({
		position: currentLocation,
		map: map,
		title:"Your location"
	});

	// Call the running tracks file and load them on the map.
	loadRunPathsJSON();
}

function loadRunPathsJSON () {
	jQuery.getJSON('js/runningTracks.json', calcRoute);
}

function calcRoute(runningTracks) {
	console.log(runningTracks);
	for (var i = 0; i < runningTracks.length; i +=1) {
		console.log(runningTracks[i]);
		var track = runningTracks[i];
		addTrack(track);
	}
}

function addTrack(track) {
	var start = new google.maps.LatLng(track.begin.lat, track.begin.lng);
	var end = new google.maps.LatLng(track.end.lat, track.end.lng);
	
	var request = {
		origin:start,
		destination:end,
		waypoints:[],
		travelMode: google.maps.TravelMode.WALKING
	};

	for (var i = 0; i < track.waypoints.length; i += 1) {
		request.waypoints.push({
			'location': new google.maps.LatLng(track.waypoints[i].lat, track.waypoints[i].lng),
			'stopover': false
		});
	}
	var directionsService = new google.maps.DirectionsService();
	var directionsDisplay = new google.maps.DirectionsRenderer();
	directionsDisplay.setMap(map);

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response);
		}
	});
}